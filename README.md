# Intro_to_bootstrap


In the file provided there are :

- A plot that compare empirical and theoretical distribution functions


- A simulation using uniform distribution to have empirical and theoretical frequences


- The use of empirical bootstrap to calculate confidence interval on the mean


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

November 2021
